import java.util.HashMap;

/**
 * Choice is a useful class that consists of both a ChoiceType enumeration and a Choice class. It turns input into a unit
 * of action for easier manipulations of program state.
 */

/**
 * Enumeration Class - helps with control flow.
 */
enum ChoiceType
{
    QUIT, RESTOCK, MAKEDRINK;
}

/**
 * The class Choice keeps track of both the type of Choice made (ChoiceType) and a selection made for the drink.
 */
public class Choice
{
    //Not final because choices can be changed and reassigned.
    private ChoiceType choiceType;
    private Integer selectionNumber;

    //Private constructor, so that the implementation is encapsulated for the classes that uses objects of this class.
    //Set to private so that the object is wrapped up properly. Sets everything to null to start.
    private Choice()
    {
        choiceType = null;
        selectionNumber = null;
    }

    /**
     * Gets selection number.
     *
     * @return the selection number
     */
    public Integer getSelectionNumber()
    {
        return selectionNumber;
    }

    /**
     * Sets choice type.
     *
     * @param choiceType the choice type
     */
    public void setChoiceType(ChoiceType choiceType)
    {
        this.choiceType = choiceType;
    }

    /**
     * Sets selection number.
     *
     * @param selectionNumber the selection number
     */
    public void setSelectionNumber(Integer selectionNumber)
    {
        this.selectionNumber = selectionNumber;
    }

    /**
     * Gets choice type.
     *
     * @return the choice type
     */
    public ChoiceType getChoiceType()
    {
        return this.choiceType;
    }

    /**
     * This function is used to get a blank choice. It returns an unset version of the getChoice() method, and allows
     * the CoffeeMachine to differentiate between NULL choices and choices that were invalid and therefore undecided.
     * @return the unchosen choice
     */
    public static Choice getUnchosenChoice()
    {
        return new Choice();
    }

    /**
     * This method converts stringed input to an actual choice, and returns a Choice object that has both the
     * type of choice and the selected drink number (if it it's not RESTOCK OR QUIT).
     *
     * Static so that it generates a Choice based on input.
     *
     * @param input the input
     * @return the choice
     */
    public static Choice getChoice(String input)
    {
        //Create a new choice to be returned.
        Choice myChoice = new Choice();

        //Takes user input and makes it lower case to have uniform parsing.
        input = input.toLowerCase();

        //If the input is quit, then we return a choice with a ChoiceType of QUIT.
        if (input.equals("q"))
        {
            myChoice.setChoiceType(ChoiceType.QUIT);
            return myChoice;
        }

        //If the input is restock, then we return a choice with a ChoiceType of RESTOCK.
        else if (input.equals("r"))
        {
            myChoice.setChoiceType(ChoiceType.RESTOCK);
            return myChoice;
        }

        //Gets a list of recipes.
        HashMap<Integer, Drink> recipeList = RecipeBook.getInstance().getRecipeList();
        try
        {
            //Takes the input and tries to parse it to a number. If it fails, an exception is thrown and we return a null object.
            Integer choiceNumber = Integer.parseInt(input);

            //We check to see that the number is a key in the menu. Only then do we return an encapsulated choice, otherwise
            //we return a null object.
            if (recipeList.containsKey(choiceNumber))
            {
                myChoice.setChoiceType(ChoiceType.MAKEDRINK);
                myChoice.setSelectionNumber(choiceNumber);
                return myChoice;
            }
            return null;
        }
        catch (NumberFormatException n)
        {
            return null;
        }
    }
}
