import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * The class Drink. This is essentially the same as a Recipe/MenuItem class. Contains a recipe field as well as name field.
 *
 * This class is Hashable, since it is used to index the RecipeBook's HashMap data structure.
 */
public class Drink
{
    //Final, since the recipe and drinkName should not be changed.
    private final HashMap<Ingredient, Integer> recipe;
    private final String drinkName;

    /**
     * Instantiates a new Drink.
     *
     * @param drinkName the drink name
     */
    public Drink(String drinkName)
    {
        this.drinkName = drinkName;
        this.recipe = new HashMap<>();
    }

    /**
     * Add to recipe. This must be done or else the recipe Map will be empty.
     *
     * @param nameOfIngredient the name of ingredient
     * @param unitsNeeded      the units needed
     */
    public void addToRecipe(String nameOfIngredient, int unitsNeeded)
    {
        this.recipe.put(IngredientList.getIngredientByName(nameOfIngredient), unitsNeeded);
    }

    /**
     * Gets recipe.
     *
     * @return the recipe
     */
    public HashMap<Ingredient, Integer> getRecipe()
    {
        return this.recipe;
    }

    /**
     * Gets drink name.
     *
     * @return the drink name
     */
    public String getDrinkName()
    {
        return this.drinkName;
    }

    //Override hashCode() since this class is used in HashMap
    @Override
    public int hashCode()
    {
        return recipe.hashCode() + drinkName.hashCode();
    }

    //Override equals() since this class is used in HashMap
    @Override
    public boolean equals(Object o)
    {
        if (o == null)
            return false;
        else
        {
            if (o instanceof Drink)
            {
                Drink that = (Drink) o;
                if (that.getDrinkName().equals(this.drinkName) &&
                        this.drinkName.equals(that.getDrinkName()) &&
                        that.getRecipe().equals(this.recipe) &&
                        this.recipe.equals(that.getRecipe()))
                    return true;
            }
            return false;
        }
    }

    /**
     * Calculates the cost of the entire drink. We need to iterate through all the ingredients and how many of each
     * are required.
     *
     * @return the big decimal
     */
    public BigDecimal calculateCost()
    {
        //Sets initial cost to 0.
        BigDecimal cost = new BigDecimal(new BigInteger("00"),2);

        Iterator it = recipe.entrySet().iterator();

        //For every ingredient
        while (it.hasNext())
        {
            //We downcast
            Map.Entry<Ingredient, Integer> pair = (Map.Entry) it.next();

            //And add to the current cost the product of unitsNeeded for the ingredient and its cost.
            //Integer.toString(pair.getValue())+"00") converts something like "3" => "300", which is then turned to 3.00 as a BigDecimal.
            cost = cost.add(pair.getKey().getCost().multiply(new BigDecimal(new BigInteger(Integer.toString(pair.getValue())+"00"),2)));
        }
        return cost;
    }
}
