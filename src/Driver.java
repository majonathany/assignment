/**
 * This file contains the test driver for the project.
 *
 * Nothing further is needed to start the program other than running it.
 */
public class Driver
{
    public static void main(String[] args)
    {
        //Creates a singleton instance of the coffee machine
        CoffeeMachine coffeeMachine = CoffeeMachine.getInstance();

        //Runs the machine until q is pressed - contains a loop.
        coffeeMachine.operate();
    }
}
