/**
CoffeeMachine is a singleton class - the instance is wrapped up inside the class and getInstance() retrieves it.

 It is the highest level class - it depends on all others. Contains the method operate, and two helper methods:
 printMenuToConsole() and printInventoryToConsole()
 */
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

/**
 * The Class Coffee machine.
 */
public class CoffeeMachine
{
    //Singleton instance
    private static CoffeeMachine coffeeMachine;

    //Two connected objects: its inventory, and its list of recipes.
    private IngredientInventory ingredientInventory;
    private RecipeBook recipeBook;

    //private constructor for Singleton
    private CoffeeMachine()
    {
        this.ingredientInventory = new IngredientInventory();

        //RecipeBook is a singleton as well.
        this.recipeBook = RecipeBook.getInstance();
    }

    /**
     * Gets CoffeeMachine instance
     * @return the instance
     */
    public static CoffeeMachine getInstance()
    {
        //If it doesn't exist already, creates a new instance.
        if (coffeeMachine == null)
        {
            coffeeMachine = new CoffeeMachine();
            return coffeeMachine;
        }
        return coffeeMachine;
    }

    /**
     * Helper function that prints the first part of the output.
     */
    private void printInventoryToConsole()
    {
        System.out.println("Inventory:");

        //Create an iterator to go through all ingredients, since the IngredientInventory uses a HashMap data structure for performance.
        Iterator it = ingredientInventory.getIngredientsPresent().entrySet().iterator();

        //For all ingredients:
        while (it.hasNext()) {

            //We downcast to specific elements and print <IngredientName> <Units Present>.
            Map.Entry<Ingredient, Integer> ingredient = (Map.Entry) it.next();
            System.out.println(ingredient.getKey().getIngredientName() + "," + Integer.toString(ingredient.getValue().intValue()));
        }
    }

    /**
     * Helper function that prints the second part of the output.
     */
    private void printMenuToConsole()
    {
        System.out.println("Menu:");

        //Create an iterator to go through all recipes, since the RecipeBook uses a HashMap data structure for performance.
        Iterator it = recipeBook.getRecipeList().entrySet().iterator();

        //For all recipes:
        while (it.hasNext())
        {
            //We downcast to specific elements and print out <MenuNumber> <DrinkName> <Cost> <Indicator>
            Map.Entry<Integer, Drink> drink = (Map.Entry) it.next();

            //Checks to see if output is like 2.9, and converts to 2.90
            String cost = drink.getValue().calculateCost().doubleValue()*10%1==0?
                    Double.toString(drink.getValue().calculateCost().doubleValue())+"0":
                    Double.toString(drink.getValue().calculateCost().doubleValue());

            String canPerform = Boolean.toString(ingredientInventory.canPerformTransaction(drink.getValue()));

            System.out.println(drink.getKey().toString() + "," +
                    drink.getValue().getDrinkName() + ",$" +
                    cost + "," + canPerform);
        }

    }

    /**
     * Operates the coffee machine:
     *      1) Creates a scanner using System.in
     *      2) While the choice is not QUIT
     *          3) Enter a command
     *              4) If input is not valid, userChoice will be null and the loop will begin again.
     *              5) If input is Q/q, we set the choice to QUIT so that the loop ends and the program ends.
     *              6) If input is RESTOCK, we restock and start again.
     *              7) If the input is a drink number, we perform the transaction if it's performable, else we write back
     *              saying it is OUT OF STOCK.
     */
    public void operate()
    {
        //Creates a scanner for System.in
        Scanner scanner = new Scanner(System.in);

        //Creates a userChoice that has all values set to NULL, but it itself is NOT null.
        Choice userChoice = Choice.getUnchosenChoice();

        //For user input
        String input;

        //Loops until QUIT is selected.
        while (userChoice.getChoiceType() != ChoiceType.QUIT)
        {
            //Prints starting output before every selection.
            printInventoryToConsole();
            printMenuToConsole();

            //Gets input and tries to generate a choice.
            System.out.print("Please enter a command (q/r/#): ");
            input = scanner.next();

            //If the choice is valid, then it will be QUIT, REMOVE OR MAKEDRINK. Else it will be NULL.
            userChoice = Choice.getChoice(input);

            //If it's null due to invalid input, then we change userChoice from NULL (set on previous line) to NON-NULL
            //with null fields to avoid NullPointerException.
            if (userChoice == null)
            {
                System.out.print("Invalid Selection: "+input+"\n");
                userChoice = Choice.getUnchosenChoice();
            }

            //If choice is QUIT, then we skip to end of loop and terminate.
            else if(userChoice.getChoiceType() == ChoiceType.QUIT)
            {
                userChoice.setChoiceType(ChoiceType.QUIT);
            }

            //Else, we do another type of operation.
            else
            {
                //We restock by calling restockInventory() if choice is RESTOCK
                if (userChoice.getChoiceType() == ChoiceType.RESTOCK)
                {
                    ingredientInventory.restockInventory();

                }

                //By this time, the userChoice MUST BE one of the 6 types of drinks. This is already checked by the
                //userChoice = Choice.getChoice(input) line, which checks if the INTEGER is one of the ones listed in
                //the menu.
                else
                {
                    //drinkNumber CANNOT be <1 and >6. Or else userChoice would have been null.
                    //Obtains the kind of drink by querying the data structure inside recipeBook.
                    int drinkNumber = userChoice.getSelectionNumber();
                    Drink drink = recipeBook.getDrinkByNumber(drinkNumber);

                    //Checks to see if the transaction can be performed. If so, then it performs it.
                    if (ingredientInventory.canPerformTransaction(drink))
                        ingredientInventory.performTransaction(drink);

                    //Else, prints an OUT OF STOCK message and changes userChoice from NULL to NON-NULL with NULL fields
                    //so that a NullPointerException is not thrown in the while loop.
                    else
                        {
                        System.out.print("Out of Stock: "+ drink.getDrinkName()+"\n");
                        userChoice = Choice.getUnchosenChoice();
                    }
                }
            }
        }
    }
}
