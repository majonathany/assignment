import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * IngredientInventory keeps track of all the ingredients in the CoffeeMachine and how many units are present.
 */
public class IngredientInventory
{
    /*
    HashMap is used to keep track of the number of units present for each ingredient. HashMap is used for constant retrieval and
    input. It is final because there is no desire to change original data.
     */
    private final HashMap<Ingredient, Integer> ingredientsPresent;

    /**
     * Instantiates a new Ingredient inventory.
     */
    public IngredientInventory()
    {
        ingredientsPresent = new HashMap<>();

        initializeIngredientInventory();
    }

    //Takes the ingredient list and adds 10 of each.
    private void initializeIngredientInventory()
    {
        for (Ingredient i: IngredientList.getIngredientList())
        {
            ingredientsPresent.put(i, 10);
        }
    }
    /**
     * Gets ingredients present.
     *
     * @return the ingredients present
     */
    public HashMap<Ingredient, Integer> getIngredientsPresent()
    {
        return this.ingredientsPresent;
    }

    /**
     * Restock inventory. Clears the inventory first (not reassigning due to final), and then initializes the inventory
     * to make everything 10.
     */
    public void restockInventory()
    {
        ingredientsPresent.clear();

        initializeIngredientInventory();
    }

    /**
     * Perform transaction. Only a generic drink is needed to perform the transaction.
     *
     * @param drink the drink
     */
    public void performTransaction(Drink drink)
    {
        //We need an iterator to access the drink's recipe's data structure.
        Iterator it = drink.getRecipe().entrySet().iterator();

        //For each ingredient in the recipe,
        while (it.hasNext())
        {
            //IngredientUnderConsideration is the ingredient in the recipe we need to subtract from. We use it
            //to get access to the inventory's ingredient count.
            Map.Entry<Ingredient, Integer> ingredientUnderConsideration = (Map.Entry) it.next();

            //Perform an inventory change, subtracting the amount present from the amount needed:
            ingredientsPresent.replace(ingredientUnderConsideration.getKey(),
                    ingredientsPresent.get(ingredientUnderConsideration.getKey())- ingredientUnderConsideration.getValue());
        }

        //Message to the user.
        System.out.println("Dispensing: " + drink.getDrinkName());
    }

    /**
     * Returns true or false depending on whether the transaction can be performed or the ingredients are out of stock.
     *
     * @param drink the drink
     * @return the boolean
     */
    public boolean canPerformTransaction(Drink drink)
    {
        Iterator it = drink.getRecipe().entrySet().iterator();

        //For each ingredient in the recipe,
        while (it.hasNext())
        {
            Map.Entry<Ingredient, Integer> ingredientUnderConsideration = (Map.Entry) it.next();

            //If the amount it requires is greater than the amount we currently have in stock, we return false.
            if (ingredientUnderConsideration.getValue().intValue() > ingredientsPresent.get(ingredientUnderConsideration.getKey()).intValue())
                return false;
        }

        //else return true
        return true;
    }
}
