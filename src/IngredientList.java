import java.util.ArrayList;

/**
 * This is a helper loader class. Adds all the ingredients and gets the list. Ingredients can be added here, and recipes
 * can be added in the RecipeBook class. In the real world, this would be stored in a database.
 *
 * You cannot instantiate this class, everything is static.
 */

public class IngredientList
{
    //Returns a list of ingredients.
    public static ArrayList<Ingredient> getIngredientList()
    {
        ArrayList<Ingredient> ingredientList = new ArrayList<>();

        ingredientList.add(new Ingredient("Coffee", "075"));
        ingredientList.add(new Ingredient("Decaf Coffee", "075"));
        ingredientList.add(new Ingredient("Sugar", "025"));
        ingredientList.add(new Ingredient("Cream", "025"));
        ingredientList.add(new Ingredient("Steamed Milk", "035"));
        ingredientList.add(new Ingredient("Foamed Milk", "035"));
        ingredientList.add(new Ingredient("Espresso", "110"));
        ingredientList.add(new Ingredient("Cocoa", "090"));
        ingredientList.add(new Ingredient("Whipped Cream", "100"));

        return ingredientList;
    }

    //Gets the ingredient by name through iteration. If it does not exist, null is returned.
    public static Ingredient getIngredientByName(String name)
    {
        for (Ingredient i: getIngredientList())
            if (i.getIngredientName().toLowerCase().equals(name.toLowerCase()))
                return i;
        return null;
    }
}
