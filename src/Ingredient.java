import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * This is the lowest class in the program - it contains two fields, the ingredient's name and the cost per unit of
 * the ingredient.
 *
 * BigDecimals were used instead of doubles because they handle currency arithmetic more precisely than doubles.
 *
 * This class is hashable - this is why hashCode() and equals() are implementing. This class will be the key for
 * the IngredientInventory's data structure.
 */
public class Ingredient
{
    //Set to final because an ingredient has a set name and cost.
    private final String ingredientName;
    private final BigDecimal cost;

    /**
     * Instantiates a new Ingredient.
     *
     * @param ingredientName the ingredient name
     * @param cost           the cost
     */
    public Ingredient (String ingredientName, String cost)
    {
        this.ingredientName = ingredientName;
        this.cost = new BigDecimal(new BigInteger(cost), 2);

        //BigIntegers convert Strings to integers, and then can be converting to BigDecimals with the scale parameter.
        //2 means that there are two decimals points on the right side.
    }

    //Need to override equals() for hashing.
    @Override
    public boolean equals(Object o)
    {
        if (o == null)
            return false;
        else
        {
            if (o instanceof Ingredient)
            {
                Ingredient that = (Ingredient) o;
                return this.ingredientName.equals(that.getIngredientName()) && this.cost.equals(that.getCost());
            }
            return false;
        }
    }

    //Need to override hashCode() for hashing.
    @Override
    public int hashCode()
    {
        return this.ingredientName.hashCode() + this.cost.hashCode();
    }

    /**
     * Getter method that gets the ingredient's name.
     *
     * @return the ingredient name
     */
    public String getIngredientName()
    {
        return this.ingredientName;
    }

    /**
     * Getter method that gets the cost for ingredient..
     *
     * @return the cost
     */
    public BigDecimal getCost()
    {
        return this.cost;
    }
}
