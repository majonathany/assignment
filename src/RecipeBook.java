import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * This class contains all the types of items that can be created in the menu. Stores the Drink along with an assigned
 * drinkNumber in the HashMap<Integer, Drink> recipeList.
 *
 * This class is a singleton, since there is only one recipe book.
 */
public class RecipeBook
{
    /**
     * The Recipe list. The key represents its number in the menu, and the Drink represents the name of the drink.
     */
    HashMap<Integer, Drink> recipeList;

    //Singleton instance
    private static RecipeBook recipeBookInstance = null;

    //Constructor that sets up the recipeBook.
    private RecipeBook()
    {
        initializeRecipeBook();
    }

    /**
     * Gets singleton instance.
     *
     * @return the instance
     */
    public static RecipeBook getInstance()
    {
        if (recipeBookInstance == null)
        {
            recipeBookInstance = new RecipeBook();
            return recipeBookInstance;
        }
        else
            return recipeBookInstance;
    }

    //Adds items to the recipeBook list. Creates an indexer i which represents its menu number.
    private void initializeRecipeBook()
    {
        //Initializes the hashmap
        recipeList = new HashMap<>();

        //Starts from 1 as the KEY not the index.
        int i = 1;
        ArrayList<Drink> listOfDrinks = getStartingRecipes(); //gets a list of all recipes using the helper function getStartingRecipes()

        for (Drink drink: listOfDrinks)
        {
            recipeList.put(i++, drink); //i++ means it starts from 1, not 2.
        }
    }

    /**Loads data. This can be easily modified to add more recipes. In the real world, this would be replaced by a
     * database and accessed through ORM.
     *
     * @return ArrayList of drinks
     */
    private ArrayList<Drink> getStartingRecipes()
    {
        //Step 1: Create the Drink object.
        Drink coffee = new Drink("Coffee");
        Drink decafCoffee = new Drink("Decaf Coffee");
        Drink caffeLatte = new Drink("Caffe Latte");
        Drink caffeAmericano = new Drink("Caffe Americano");
        Drink caffeMocha = new Drink("Caffe Mocha");
        Drink cappucino = new Drink("Cappucino");

        //Step 2: Set its ingredients

        //Coffee Recipe
        coffee.addToRecipe("coffee", 3);
        coffee.addToRecipe("sugar", 1);
        coffee.addToRecipe("cream",1);

        //Decaf Coffee Recipe
        decafCoffee.addToRecipe("decaf coffee", 3);
        decafCoffee.addToRecipe("sugar", 1);
        decafCoffee.addToRecipe("cream", 1);

        //Caffe Latte
        caffeLatte.addToRecipe("espresso", 2);
        caffeLatte.addToRecipe("steamed milk", 1);

        //Caffe Americano
        caffeAmericano.addToRecipe("espresso", 3);

        //Mocha
        caffeMocha.addToRecipe("espresso", 1);
        caffeMocha.addToRecipe("cocoa", 1);
        caffeMocha.addToRecipe("steamed milk", 1);
        caffeMocha.addToRecipe("whipped cream", 1);

        //Cappucino
        cappucino.addToRecipe("espresso", 2);
        cappucino.addToRecipe("steamed milk", 1);
        cappucino.addToRecipe("foamed milk", 1);

        //Step 3: Create a list and add the drink to it.
        ArrayList<Drink> listOfDrinks = new ArrayList<>();

        listOfDrinks.add(coffee);
        listOfDrinks.add(decafCoffee);
        listOfDrinks.add(caffeLatte);
        listOfDrinks.add(caffeAmericano);
        listOfDrinks.add(caffeMocha);
        listOfDrinks.add(cappucino);

        //Step 4: Return the list.
        return listOfDrinks;
    }

    /**
     * Gets recipe list.
     *
     * @return the recipe list
     */
    public HashMap<Integer, Drink> getRecipeList()
    {
        return this.recipeList;
    }

    /**
     * Gets drink by name. Must iterate through all elements of the hashmap.
     *
     * @param name the name
     * @return the drink by name
     */
    public Drink getDrinkByName(String name)
    {
        Iterator it = recipeList.entrySet().iterator();
        while (it.hasNext())
        {
            Map.Entry<Integer, Drink> pair = (Map.Entry) it.next();
            if (pair.getValue().getDrinkName().toLowerCase().equals(name))
                return pair.getValue();
        }
        return null;
    }

    /**
     * Gets drink by number. Does not need iteration since we just hash the integer to get the value for the key.
     *
     * @param i the
     * @return the drink by number
     */
    public Drink getDrinkByNumber(int i)
    {
        return recipeList.get(i);
    }
}
