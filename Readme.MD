<h1>Barista-Matic Coffee Machine Simulator</h1>

<h3>By: Jonathan Ma</h3>

+ This program was written in IntelliJ with JDK 1.8 and JUnit, so I recommend you use it to check it.

+ Everything works properly. The only slight deviation from the PDF's specifications is that you
can see what you're inputting character-wise and there is a prompt that says:
    + "Please enter a command (q/r/#): "
        + This prompt is necessary so the user can input their choice.
        + This is necessary because in order to input commands without the prompt, I would have
        had to use a third party library and a keyListener.

+ UML text.txt specifies the class design, and was a document I wrote prior to starting the project
that helped outline what needed to be done to make the project.

+ There are two JUnit tests in the /tests folder. They test to see if the BigDecimal class
I used for currency works properly, and that the Drink class's hash table produces the correct output.

+ The menu items and the ingredients are out of order, because I used a HashMap instead of a TreeMap.

+ **The Driver class contains the main function.**

Messages:

+ This was a useful exercise. There was a confusing example in the bottom of the PDF however: it showed
the commands 2 and q in succession, as if that were one command. I didn't understand how you could
put 2 commands at once, but I imagine this is a small typo because earlier it says you can only put one
command at a time. So I went with the one command at a time specification.

<h1>I hope I move on to the next round!</h1>