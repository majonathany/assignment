File that helped me organize the class design

Class Design:
+: public
-: private

Driver (MAIN)

ChoiceType (enumeration)
    ~ QUIT
    ~ RESTOCK
    ~ MAKEDRINK

Choice
    - ChoiceType choiceType
    - int selectionNumber
    + int getSelectionNumber()
    + void setSelectionNumber(int selectionNumber)
    + ChoiceType getChoiceType()
    + Void setChoiceType(ChoiceType choiceType)
    + static Choice getUnchosenChoice()
    + static Choice getChoice(String input)

CoffeeMachine (Singleton)
    - IngredientInventory ingredientInventory
    - RecipeBook recipeBook
    - static CoffeeMachine coffeeMachine
    + void operate ()
    - void printInventoryToConsole()
    - void printMenuToConsole()
    + static CoffeeMachine getInstance()

IngredientInventory
    - HashMap (Ingredient ingredient => int unitsPresent) ingredientsPresent
    + HashMap<Ingredient, Integer> getIngredientsPresent()
    + void restockInventory
    + void performTransaction (Drink drink)
    - boolean canPerformTransaction (Drink drink)

RecipeBook (Singleton)
    - static RecipeBook instance
    - HashMap<Integer, Drink> recipeList
    - HashMap<Integer recipeNumber, Drink drink> recipeList
    - static RecipeBook getInstance()
    - void initializeRecipeBook()
    + ArrayList<Drink> getStartingRecipes()
    + Drink getDrinkByName(String name)
    + Drink getDrinkByNumber(Integer number)
    + HashMap<Integer, Drink> getRecipeList()

Drink (Hashable)
    - String drinkName
    - HashMap<Ingredient ingredient, Integer numberOfUnitsNeeded>  recipe
    - int getUnitsNeeded (Ingredient ingredient)
    + HashMap<Ingredient, Integer> getRecipe()
    + String getRecipeName()
    + void addToRecipe (String nameOfIngredient, int unitsNeeded)

IngredientList
    + static ArrayList<Ingredient> getIngredientList()
    + static Ingredient getIngredientByName(String nameOfIngredient)

Ingredient (Hashable)
    - String ingredientName
    - BigDecimal cost
    + getIngredientName()
    + getIngredientCost()