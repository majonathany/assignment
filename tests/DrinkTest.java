import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by majonathany on 4/25/17.
 */
public class DrinkTest
{
    @Test
    public void testCostFunction1() throws Exception
    {
        Drink drink = RecipeBook.getInstance().getDrinkByName("coffee");

        System.out.println(drink.calculateCost().doubleValue());

        assertTrue(drink.calculateCost().doubleValue() == 2.75);
    }

    @Test
    public void testCostFunction2() throws Exception
    {
        Drink drink = RecipeBook.getInstance().getDrinkByName("decaf coffee");

        System.out.println(drink.calculateCost().doubleValue());

        assertTrue(drink.calculateCost().doubleValue() == 2.75);
    }

    @Test
    public void testCostFunction3() throws Exception
    {
        Drink drink = RecipeBook.getInstance().getDrinkByName("caffe americano");

        System.out.println(drink.calculateCost().doubleValue());

        assertTrue(drink.calculateCost().doubleValue() == 3.30);
    }

    @Test
    public void testCostFunction4() throws Exception
    {
        Drink drink = RecipeBook.getInstance().getDrinkByName("caffe mocha");

        System.out.println(drink.calculateCost().doubleValue());

        assertTrue(drink.calculateCost().doubleValue() == 3.35);
    }

    @Test
    public void testCostFunction5() throws Exception
    {
        Drink drink = RecipeBook.getInstance().getDrinkByName("caffe latte");

        System.out.println(drink.calculateCost().doubleValue());

        assertTrue(drink.calculateCost().doubleValue() == 2.55);
    }

    @Test
    public void testCostFunction6() throws Exception
    {
        Drink drink = RecipeBook.getInstance().getDrinkByName("cappucino");

        System.out.println(drink.calculateCost().doubleValue());

        assertTrue(drink.calculateCost().doubleValue() == 2.90);
    }

}