import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;

import static org.junit.Assert.*;

public class IngredientTest
{
    BigDecimal myDecimal;

    @Before
    public void setUp() throws Exception
    {
        myDecimal = new BigDecimal(new BigInteger("207"), 2);
    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testConstructor() throws Exception
    {
        assertTrue(new Ingredient ("Cinnamon", "075").getCost().doubleValue() ==
                new BigDecimal(.75).doubleValue());
    }

    @Test
    public void testCurrency() throws Exception
    {
        assertTrue(2.07 == myDecimal.doubleValue());
    }

    @Test
    public void testAdd() throws Exception
    {
        assertTrue(3.51 == myDecimal.add(new BigDecimal(1.44)).doubleValue());
    }

    @Test
    public void testDivide() throws Exception
    {
        myDecimal = new BigDecimal(new BigInteger("2000"), 2);
        assertTrue(100.00 == myDecimal.divide(new BigDecimal(new BigInteger("20"), 2)).doubleValue());
    }

    @Test
    public void testHashCode() throws Exception
    {
        BigDecimal myDecimal1 = new BigDecimal(new BigInteger("2177"), 2);
        BigDecimal myDecimal2 = new BigDecimal(new BigInteger("2177"), 2);
        assertTrue(myDecimal1.hashCode() == myDecimal2.hashCode());
    }

    @Test
    public void testLessThanZero() throws Exception
    {
        Ingredient ingredient = new Ingredient("Coffee", "075");
        assertTrue(ingredient.getCost().doubleValue() == .75);
    }

    @Test
    public void testInts() throws Exception
    {
        assertTrue(new BigDecimal(new BigInteger("500"),2).multiply(new BigDecimal(new BigInteger("200"),2)).doubleValue() == 10);
    }

}